---
title: "Guvernul actual vrea ca Fiscul să știe de ce-i trimiți bani soției tale sub formă de bitcoin dacă ești furnizor de portofele digitale și exchange"
date: 2020-04-17T21:58:27+03:00
draft: false
tags: ["Libertate", "Criptomonede", "OpiniePersonala", "INFORMAL" ]
---

***O notă**: Bineînțeles că e un titlu imbecil în stil *clickbait* și la mișto, dar de aceea am lămurit ce e aia etichetă de **#INFORMAL** în categoria [**Despre Blog**](/static-post/despre)*

Guvernul vrea ca Ministrul Finanțelor să aprobe dacă poți sau nu să-i trimiți nevestei tale bani tranzacționați în condițiile în care tu deții o firmă de tranzacționare monedă virtuală în RON sau invers

*Implicit dacă deții și ATM-uri care fac serviciu de tranzacționare*

<!--more-->



Așadar, avem o nouă știre: **[aici](https://www.startupcafe.ro/afaceri/criptomonede-proiect-oug-reglementare-exchange-autorizatie-ministerul-finantelor.htm)**

Guvernul crede că există vreo firmă de tranzacționare de criptomonede de la noi din țară care face serviciu de tranzacții crypto în RON și invers.

Se pare că mai trebuie să caute deoarece și noi ăștia care vrem sa cumpărăm și să tranzacționăm, vrem să știm.

De altfel, legea e absolut inutilă. Un showoff cu mușchi.

Noa asta e, dragi compatrioți: De ce veți vrea să faceți schimb de crypto în dolari și de ce în Singapore?