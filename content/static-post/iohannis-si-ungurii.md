---
title: "Iohannis latră la pesediști din cauza udemeriștilor cu áutőnőmiá lor"
date: 2020-04-29T15:34:26+03:00
draft: false
tags: [ "INFORMAL", "OpiniePersonala", "In curs de revizuire" ]
---

*Evident că e titlu stil clickbait. Citește [despre eticheta de #INFORMAL.](/static-post/despre)*

### Dar asta nu mă oprește să râd la noul schendăl din Pőrlőméntúl Rúmâniéi

<!--more-->

M-a lovit știrea de dimineață cam așa: https://www.digi24.ro/stiri/actualitate/klaus-iohannis-jo-napot-kivanok-ciolacu-este-incredibil-ce-intelegeri-se-fac-in-parlament-psd-se-lupta-ca-sa-dea-ardealul-ungurilor-1299251


Contextul este unul evident. În cursul zilei de ieri 28.04.2020, în Camera Deputaților a fost votată tacit o inițiativă care *ar putea* da autonomie Ținutului Secuiesc.

Detalii mai multe aici: https://www.digi24.ro/stiri/actualitate/politica/autonomia-tinutului-secuiesc-adoptata-tacit-de-camera-deputatilor-ce-prevede-proiectul-1299077

Această inițiativă propune extinderea land-ului secuiesc într-un soi de autonomie teritorială pe criterii istorice revizioniste maghiare.

Propunerea a fost votată de către ~~PSD-ul unguresc~~ UDMR și PSD printr-o majoritate ușoară (majoritate simplă).

Bineînțeles că inclusiv titlul de la Digi24 e *alarmist*, deoarece este doar un proiect de lege și nicidecum ceva bătut în cuie din punct de vedere constituțional. Trebuie să treacă prin Senat întâi și ulterior la cererea președintelui legea *poate* să fie întoarsă spre CCR pentru analiza constituționalității.

Momentan CCRul are majoritatea judecătorilor puși de vechea coaliție parlamentară formată din PSD+ALDE+UDMR. Deci, o să râdem puțin și acolo. Dacă CCRul zice că e constituțională legea autonomiei, deja inclusiv Ardealul poate să zică szérusz liniștit vechiului regat.

### Însă hai să ne concentrăm un pic pe propunerile proiectului

Mă refer la site-ul Camerei Deputaților: http://www.cdep.ro/pls/proiecte/upl_pck2015.proiect?idp=18286

Întâi și întâi trebuie să vedem expunerea de motive aici: http://www.cdep.ro/proiecte/2019/600/70/0/em898.pdf

Citez: *„Secuimea, populația autohtonă cu identitate națională maghiară, majoritară în Ținutul Secuiesc, revendică autonomia teritorială a acestei regiuni în conformitate cu prevederile documentelor internaționale și cu practica în acest domeniu din statele Uniunii Europene (..) Autonomia scaunelor a funcționat sub formă de autonomie teritorială, având o administrare proprie, structură socială și administrativă specifică, drepturi speciale și statut special.”*

Din asta înțeleg un singur lucru și anume că doar secuimea este *populația* (atenție exprimarea) autohtonă a Ținutului Secuiesc, ceea ce este incorect din punct de vedere istoric. Populația autohtonă secuiască este la fel de autohtonă pe teritoriul *secuimii* ca și orice altă populație autohtonă. Fie că este populație autohtonă română sau de etnie rromă chiar dacă sunt minoritare în județe.

Tot din același paragraf pot să observ revizionismul istoric pentru județele considerate strict secuiești (Mureș, Covasna, Harghita). O problemă cu acest revizionism este faptul că în județul Mureș majoritatea populației este de etnie română. Și atunci, ce faci? Îi iei pe mureșeni cu japca și îi alipești la ținutul autonom? Serios? Nu le dai un pic de libertate să decidă pentru ei dacă vor să se *alipească* la secui?

Mergem mai departe. *„Locuitorii scaunelor secuiești, prin reprezentanții lor aleși își afirmă dorința și revendică dreptul la autonomie teritorială, la crearea cadrului legislativ prin adoptarea Statutului de Autonomie al Ținutului Secuiesc (..)”* adică ceea ce vor să zică aici e că inclusiv județul Mureș si-a afirmat apartenența la Ținutul Secuiesc prin reprezentanții lor aleși. Problema e: unde bunul D-zeu și-au afirmat mureșenii așa ceva? Nu am văzut nicăieri așa ceva.

Chiar dacă autonomia **nu se referă la crearea unor granițe și separarea completă de România**, asta nu înseamnă nici o clipă faptul că poți să îți faci autonomia județelor sau regiunilor cum ai tu chef și pe motivație revizionistă.

Mai departe, *„Recomandarea nr. 1201/1993 a C.E., prevede la art. 11. dreptul persoanelor aparținând unei minorități naționale de a avea autorități autonome sau locale, sau statut special în acele regiuni unde sunt în majoritate și dispun de condiții istorice și teritoriale specifice*. Adică, din nou, deși maghiarii nu sunt majoritari în Mureș, reprezentanții lor se consideră majoritari în Mureș. E adevărat, totuși, că județul a făcut parte din Ținutul Secuiesc. Dar o regiune nu poate include o altă regiune decât dacă majoritatea reprezentanților din fiecare componentă a regiunii este de acord cu formarea unui *Ținut* conform modului de organizare și dezvoltare a regiunilor României.

Nu poți merge pe argumentul că *„Deși nu suntem majoritari în județul Mureș, suntem majoritari în alte doua județe și împreună cu Mureș suntem majoritari în toate trei județele”*, că așa am fi putut zice la fel de bine faptul că din Ținutul Secuiesc poate face parte și județul Dolj [care a înregistrat 1468 de voturi pentru UDMR](https://www.hotnews.ro/stiri-europarlamentare_2019-23170084-interactiv-curiosul-caz-rezultatelor-udmr-alegeri-zeci-mii-voturi-scoruri-pana-6-7-ori-mai-mari-decat-alegerile-din-2016-judete-fara-comunitati-maghiare-botosani-sunt-18-maghiari-inregistrati-oficial-.htm), deci automat trebuie să însemne că reprezentanții aleși din Dolj sunt de acord cu formarea Ținutului Secuiesc alături de Harghita și Covasna. Știu că exemplul e exagerat dar trebuie înțeles punctul de vedere.

Restul detaliilor documentului se referă la ce se angajează să respecte proiectul de lege. Una dintre prevederi este *„**recunoașterea autonomiei regionale implică loialitatea față de stat, ale cărui regiuni evoluează prin respectarea suveranității și a integrității teritoriale**”*. Această prevedere zice clar faptul că autonomia nu înseamnă segregare față de statul român, ci chiar din contră, respectarea teritoriului național prin includerea administrației ținutului secuiesc în teritoriul național. Nu e nimic specific în neregulă aici, decât constituțional.

## Propunerile de *autonomie* le consider corecte, însă motivația revizionistă o consider complet greșită. Pe lângă asta, orice propunere de lege pentru autonomie este neconstituțională.

Filosofic nu este cu nimic greșit să ceri *descentralizarea unui stat*. Dar, o descentralizare sau *autonomie* pe criterii etnice sau religioase personal mi se pare un jeg preistoric care ar fi trebuit să moară în secolul 20. Asta fără sa menționăm nici măcar o clipă faptul că este neconstituțională propunerea de lege.

Acest proiect de lege are drept motivație revizionismul istoric pe baze etnice pentru Ținutul Secuiesc fără să țină cont de condițiile istorice actuale sau de *contra-revizionism*. Prin *contra-revizionism* mă refer la faptul că orice altă etnie poate să ceară revizionism *peste* revizionismul maghiar pentru includerea regiunilor maghiare în teritoriul său cu o altă majoritate.

Spre exemplu, *contra-revizioniștii* ardeleni ar putea cere *autonomia* Ardealului prin includerea Ținutului Secuiesc motivând faptul ca Ardealul este pământ care aparține autohtonilor ardeleni. Prin această motivare ardelenii pot include în motivația pentru autonomie faptul că Ținutul Secuiesc este parte integrală istorică din Ardeal unde majoritatea ardelenilor sunt de etnie română.

Ulterior, revizioniștii [regățeni](https://dexonline.ro/definitie/reg%C4%83%C8%9Bean) pot cere crearea unei regiuni care din punct de vedere istoric aparține autohtonilor de etnie română care sunt majoritari în spațiul carpato-danubiano-pontic, incluzând județele istorice Harghita și Covasna unde autohtonii români locuiesc dinaintea secolului 12. Și tot așa.

Înțelegi unde vreau sa ajung cu asta?

Din punctul ăsta de vedere Mureșul ar fi un ghimpe în coastele Ținutului Secuiesc. Ținutul Secuiesc ar fi un ghimpe în coastele Ardealului. Ardealul ar fi un ghimpe în coastele României.

Ajungem absolut nicăieri. Consider că nu așa trebuie procedat.

## Maghiarii vor limba maternă în administrație și asta consider un lucru bun

De mult trebuia dualitatea limbilor română și maghiară în administrația publică acolo unde este cazul. Nu contest nici o clipă faptul că o minoritate așa signifiantă să aibă acces la limba maternă în administrația publică.

Dar autonomia nu este în mod exact calea spre astfel de propunere.

Autonomia presupune o *descentralizare* a statului pentru eficientizarea regiunii descentralizate sau a județului descentralizat. Ori, autonomia propusă de UDMR și PSD presupune descentralizare în funcție de etnie și nu eficiență.

Din punctul ăsta de vedere bineînțeles că PSDul *ar fi putut accepta banii lui Viktor Orban* ca să bage pe bandă rulantă autonomiile maghiare în Ardeal, așa cum sugerează Iohannis.

## Autonomiile pe criterii etnice și religioase sunt de domeniul trecutului, iar Iohannis probabil cunoaște asta deja

În sfârșit ajungem la Iohannis și declarația lui *aproape* în stil vadimist.

*Tot aproape* că următoarele declarații ale lui Iohannis vor suna ca și ale lui Vadim: „*Nu faci dumneata ordine la mine în Ardeal, șomâldoacă escroacă!”*, citându-l pe Bogdan, o cunoștință de-a mea de pe Facebook.

Iohannis clar nu știe să combată autonomia pe criterii etnice decât recurgând la afirmații vadimiste. Putea să susțină exact ceea ce scriam mai sus: ***Autonomia pe criterii etnice o consider incorectă.***

Era destul. Dar n-a făcut-o. N-a gândit-o. A sărit direct pe PSD.

În schimb, maghiarii puteau cere autonomia județului Covasna, județului Harghita. Puteau face inclusiv referendum în fiecare județ pentru descentralizare. Dar nu pe criterii etnice și statut special bazat pe etnia majoritară.

Ca și un exemplu de lipsă de criterii etnice pentru descentralizare este zona Chiapas din Mexic unde gruparea Zapatistas a propus autonomia regiunii Chiapas. Au reușit. Într-adevăr au autonomie și au reușit fără să excludă absolut nici o etnie. Chiapas este o regiune autonomă descentralizată pe criterii administrativ-politice.

Similar este și republica autonomă Rojava unde nu este specificată o etnie majoritară.

Un alt aspect și efect secundar al autonomiei pe criterii etnice este împuternicirea naționalismului etniei majoritare din județul revendicat pe baze revizioniste. Cu toții cunoaștem cazul sri-lankezilor alungați din *Ținutul Secuiesc*. Dacă extrapolăm evenimentul nefericit și judecăm întreaga populație *majoritară de etnie maghiară* din vechiul Ținut Secuiesc, ajungem foarte lejer la concluzia că sunt xenofobi și rasiști. Noa, dă-le autonomie xenofobilor și rasiștilor.

Bineînțeles, exagerez. Și nu e cazul de generalizare, sper.

### Vestea actualizată: https://www.digi24.ro/stiri/actualitate/politica/klaus-iohannis-spune-ca-fara-interventia-sa-senatul-nu-ar-fi-respins-proiectul-privind-autonomia-tinutului-secuiesc-1299498

Aparent Camera Senatului a băgat în seamă discursul vadimist a lui Iohannis. Noa, hai că asta s-o terminat destul de scurtuc.

Gata cu miștoul. Sunt dezamăgit. Putea măcar să treacă legea de Senat ca să ne distrăm un pic mai mult până ce o trimitea Iohannis spre CCR la reexaminare.

Înapoi la treabă!