---
author: "Brîndușă Ștefan Cristian"
title: "Libertăți"
date: 2020-04-10T18:57:35+03:00
draft: false
tags: [ "Libertate", "Definitie", "In curs de revizuire" ]
---

## Introducere

În această postare voi încerca sa descriu principiile ideologice ale piraților politici raportat la libertăți

<!--more-->

# Principii

* Un pirat politic are ca fundament susținerea libertăților civice ![alt text](/dove-icon.png "Liberty Dove")

* Un pirat politic susține existența religiilor, tradițiilor, tuturor sexelor, genurilor sexuale, și a orice diferențe între oameni
* Un pirat politic susține libertatea de exprimare indiferent dacă ofensează pe cineva sau nu
* Un pirat politic susține dreptul de a nu avea libertățile personale abuzate
* Un pirat politic susține existența grupurilor non-etatiste în afara statelor ca și organizare liberă fără autoritate centrală, cu principii de non-agresiune solide
* Un pirat politic nu susține abuzul de putere mai ales nu pentru restricționarea drepturilor cetățenești
* Un pirat politic susține libertatea și legalizarea circulației schimbului de fișiere prin [Internet](http://www.hotnews.ro/stiri-international-5805118-partidul-piratilor-cine-este-isi-propune.htm)