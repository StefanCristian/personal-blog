---
title: "Despre"
date: 2020-04-29T13:20:14+03:00
draft: false
tags: ["Ideologie"]
---

### Blogul lui Brîndușă Ștefan-Cristian. Este blog ideologic politic, cât și blog personal de opinii pe baza ideologiei politice pirat.

De unde termenul de *Pirat*? O definiție de bază o găsim aici: http://www.hotnews.ro/stiri-international-5805118-partidul-piratilor-cine-este-isi-propune.htm

A fi pirat politic ne referim exclusiv la apartenența unei mișcări ideologice politice denumite PIRAT sau Mișcarea PIRAT

Partidul Pirat există incă din anul 2006 și în doar câțiva ani a reușit să își concretizeze o ideologie pe nișa libertăților pe Internet.

Patru ani mai târziu, în 2010, Partidul Pirat din Suedia reușește cu destule voturi să își trimită doi europarlamentari în Parlamentul European: Amelia Andersdotter și Christian Engstrom.
Ulterior, în 2014, Julia Reda a fost votată și trimisă drept europarlamentar din partea Partidului Pirat German. În 2019, Partidul Pirat German reușește din nou să trimită un europarlamentar (Patrick Breyer) alături de Partidul Pirat Ceh care reușește să trimită cei mai mulți pirați politici dintre toate partidele pirat de până acum, în număr de 3 și anume Markéta Gregorová, Mikuláš Peksa, Marcel Kolaja.

Grupul partidelor pirat face parte din Alianța Greens/EFA și anume Alianța Verzilor Europeni împreună cu federaliștii din EFA din 2010.

Tot Partidul Pirat Ceh a câștigat primăria Praga, reprezentat prin primarul Zdeněk Hřib.

Nișa politică principală care stă la baza ideologiei pirat este opoziția față de restricțiile de libertăți pe Internet, [libertăți pe care le-am definit la categoria de libertăți](/static-post/libertate/)

După cum bine știm este ilegal să faci sharing pe Internet indiferent dacă cumperi un produs online sau nu.

Din acest motiv diverse mișcări și site-uri precum thepiratebay.org sunt interzise.

### Cu acest blog voi încerca să vă transmit propunerile piraților politici cât și voi explica ideologia politică pirat nu doar din prisma mea, dar și de la definiția de bază a ideologiei politice pirat

Îmi voi adăuga, pe lângă principiile pirat ce le voi dezvolta la categoria de Ideologie, și principiile proprii și abordarea proprie **peste** principiile ideologiei pirat fără să fie nevoie să rescriu vreun principiu ideologic descris dinainte.

Un document important care stă la baza ideologiei pirat este programul oficial al piraților politici din Europa: https://wiki.ppeu.net/doku.php?id=programme:ceep2019

Link-ul în cauză duce la site-ul oficial a piraților europeni. Poate fi utilizat ca și link de descriere și programe oficiale.

### Prin acest blog nu mă afiliez vreounui partid pirat din România! Nu mai există partid pirat în România, dar asta nu înseamnă că nu pot exista pirați politici!

Îmi propun să explic principiile pirat fără intenția de a convinge pe cineva să intre în vreun partid pirat. Scopul blogului nu este să conving cititorul să intre în vreun partid pirat, ci să aduc un pic de lumină la ceea ce propun pirații politici cât și problemele pe care incearcă pirații să le rezolve.

### Mă consider pirat politic din punct de vedere a ideologiei politice pirat. Din acest motiv am dat numele blogului în forma în care se vede acum.

Posibil să vă răspund la cateva întrebări prin această descriere. Dar, cred că măcar lămuresc un pic de ce blogul are numele de *„Piratul”*.

Dat fiind faptul că am decis să creez un blog pentru ideologie politică, nu-i pot da alt nume decât ceea ce mă reprezintă din punct de vedere politic și anume principiile ideologice pirat.

Orice alt nume nu are absolut nici o tangență cu scopul pe care mi-l propun prin blog.

Nu am creat acest blog doar din cauză că am simțit nevoia să am un blog personal. Puteam să-mi cumpăr orice alt domeniu pentru acest scop.

### Blogul nu are tangență cu pirateria. Nu este nimic ilegal aici.

Doar pentru că se numește „Piratul”, blogul meu personal nu are tangență cu pirateria. Principiile politice a piraților nu au tangență cu promovarea incălcării legii.

Principiile politice pirat au legătură cu legalizarea libertății circulației schimbului de fișiere pe Internet.

Nu folosesc nimic piratat pentru că nu îmi place și nu mă definește. Folosesc Linux deoarece este software liber și deschis.

Nici ideologia nu mă oprește să folosesc orice soft cumpărat cu o licență, indiferent daca sunt sau nu împotriva sistemului de licențiere. Ba chiar din contră: ideologia lasă libertatea de a face ce dorești cu orice soft pe care-l cumperi inclusiv.

Consider faptul că pirateria ajută producătorii de software, muzică și filme, deci din acest motiv nu doresc să susțin neapărat industria care se folosește de restricții pe Internet pentru a controla ceea ce face cumpărătorul cu produsul cumpărat.

Să *piratezi* din punctul meu de vedere înseamnă să susții produsul piratat prin răspândire organică. Eu nu doresc să susțin așa ceva. Dacă producătorul a decis să restricționeze cumpărătorul printr-un sistem [DRM](https://drm.info/what-is-drm.en.html), eu nu îl voi susține prin *piraterie*.

### Mulți se simt ofensați de ceea ce propun și explic. Dacă te ofensezi din cauza a ce scriu, îți urez baftă și drum bun afară de pe blog.

Cu excepția cazurilor în care promovez știri false (ceea ce **NU** doresc să fac!), orice postare care are etichetă cu #OpiniePersonala sau #INFORMAL are tendința să ofenseze.

Ofensa este un lucru cât se poate de natural inclusiv prin expunerea unor idei chiar și controversate.

Dacă citești acest blog și te ofensezi, îți meriți soarta și te invit să zbori de pe blog.

O să te rog, totuși, să nu îmi faci reclamă doar din cauza faptului că nu îți convine ce scriu.

A-mi face reclamă, chiar și negativă, mă ajută. Dar dacă ajungi să zici că ar trebui să fiu cenzurat doar din cauza faptului că blogul a ajuns să fie răspandit, nu uita că ai contribuit la acest fenomen!

Deși nu este blog de știri, dacă crezi că ceva este *fals* sau *știre falsă* nu mă deranjează modul în care prezinți falsitatea postării. Feedback-ul este feedback. Dar nu uita că unele postări au etichetă de *#In curs de revizuire*, explicată mai jos.

**Eu nu scriu știri**, ci opinii personale. Știrile sunt scrise de agenții oficiale de știri. Blogurile sunt site-uri cu opinii personale.

### Urmărește etichetele postărilor!

Eticheta **#INFORMAL** reprezintă un mod de prezentare informal. Îmi permit inclusiv să insult. Te rog să tratezi cu prudență și mult control emoțional articolele cu etichetă de #INFORMAL. Îmi permit să fac mișto și să promovez titluri imbecile la unele postări. Dacă ți se par titlurile false și vezi eticheta de #INFORMAL, nu mă deranjează dacă mă insulți. Ba chiar din contră: mă cam aștept. O să fac lămuriri în postări când e vorba de titluri imbecile și/sau *extravagande*.

Eticheta **#OpiniePersonala** se referă la opiniile personale pe care le am în legătură cu orice știre sau eveniment pe care îl critic sau comentez. În acest caz opiniile personale nu trebuie să înceapă morfologic și sintactic cu *„În opinia mea”*. Din acest motiv pun etichetă de #OpiniePersonala, deoarece este o **opinie personală**.

Eticheta **#InCursDeRevizuire** sau **#In curs de revizuire** se referă la faptul că o îmi permit să revin oricând este necesar la postarea respectivă. Asta nu înseamnă că postarea este stabilită. Asta nu înseamnă nici faptul că postarea nu poate fi publicată în forma nerevizuită. Orice site are uneori postări care sunt în curs de revizuire sau în curs de actualizare. Este ceva firesc. Sper că ai educația să înțelegi acest fapt.

Eticheta **#InCursDeActualizare** sau **#In curs de actualizare** se referă la faptul că postarea nu a ajuns să fie revizuită, ci este în curs de actualizare. Asta înseamnă că voi reveni la acele postări ca să completez și nu neapărat ca să înlocuiesc ceea ce am scris înainte.

Eticheta **#LaMisto** sau **#LaMișto** se referă la faptul că postarea poate să conțină exagerări prin care iau la mișto oameni, atitudini, acțiuni, păreri. Asta nu înseamnă că am un punct de vedere *acceptabil*. Sau că atitudinea mea este corectă. Ci, mai de grabă faptul că ridic o problemă la fileu.

Orice alte etichete, ca de exemplu **#Libertate**, **#Ideologie**, **#Criptomonede**, **#Ideologie Politica** reprezintă scurtături la postări care țin de principiile ideologice pirat. În momentul în care dai click pe aceste etichete ele te vor redirecționa la toate postările cu etichetele ce țin de ideologia politică pirat.

Orice postare poate avea mai multe etichete. Nu dispera. Ia-le pe rând. Unele postări vor avea vreo 20 de etichete.