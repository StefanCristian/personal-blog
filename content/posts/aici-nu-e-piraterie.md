---
title: "Ho, bă. Calmează-te. Pagina asta n-are de-a face cu pirateria!"
date: 2020-04-16T17:16:49+03:00
draft: false
tags: [ "INFORMAL", "OpiniiPersonale" ]
---

### Mă adresez ție, celui care crezi că pe pagina asta e vorba de piraterie!
### Pagina asta n-are de-a face cu pirateria. Nu îmi place să dețin lucruri ilegale pe calculator.
### Ar fi stupid să le stochez și să semnalizez pe net. De aia nici nu îmi bat capul cu ele.

<!--more-->

Dacă crezi că susțin nerespectarea legilor te înșeli cât se poate de tare. Ți se pare că arăt a revoluționar?

### Nu susțin încălcarea legii. Susțin *schimbarea* legilor prin mijloace democratice.

### Nu mă interesează să dețin ilegal ceea ce nu susțin.

### Nu susțin licențele [EULA](https://en.wikipedia.org/wiki/End-user_license_agreement).

### Nu susțin [DRM](https://en.wikipedia.org/wiki/Digital_rights_management).
DRM trebuie cândva să dea cu capul de pâmânt. Măcar pentru amuzament.

### Susțin [softul liber](https://ro.wikipedia.org/wiki/Software_liber)

### Susțin softuri cu [sursă deschisă](https://ro.wikipedia.org/wiki/Surs%C4%83_deschis%C4%83)

### Doar pentru că scrie "piratul", asta nu înseamnă că dau share la chestii despre piraterie sau la site-uri cu conținut piratat. N-o să fac de astea. Nu mă interesează.

### Cel mult citesc de pe [TorrentFreak](https://torrentfreak.com/), care e doar o agenție de știri.

### Doar pentru că am principiile de mai sus, asta nu mă restricționează ideologic să cumpăr licențe pentru programe. Îmi place sa cumpăr programe și licențe pentru că îmi place să *dețin*. Îmi place să respect legea. 

### Dar asta nu înseamnă că actualul sistem de licențiere a softurilor este vreo clipă corect! Trebuie schimbat prin mijloace democratice!


Dacă te încăpățânezi să nu înțelegi faptul că o ideologie politică democratică presupune *schimbarea* legilor și **nicidecum** încălcarea lor, e clar. Trebuie să reiei ciclul gimnazial până la liceu.