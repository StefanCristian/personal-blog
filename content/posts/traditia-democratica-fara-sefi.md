---
title: "Lipsa tradiției de a ne organiza singuri ne face incapabili, iar liderul puternic dăunează"
date: 2020-04-13T16:00:44+03:00
draft: false
tags: [ "INFORMAL", "OpiniePersonala", "In curs de revizuire" ]
---

**Atenție! Aceasta este o postare informală!**

O să o ducem bine în momentul în care invățăm să ne organizăm singuri, fără nevoia ordinelor de la centru. Nu contează dacă e vorba de firme sau de organizații non-profit și nici nu e nevoie să mă leg de modul organizării locale, individuale sau comunitare, ci strict de: avem sau nu, defapt, nevoie de ajutorul statului central sau al șefilor?

<!--more-->

### În această postare mă voi lega de doua probleme **existențiale** românești.

* Problema 1) Majoritatea românilor cred că **doar un lider puternic ne va salva** ( a se citi: ne va organiza sau ne va conduce eficient)
* Problema 2) Majoritatea românilor defapt habar nu au **cum** să se salveze singuri (a se citi: nu se știu organiza singuri)

### **[Sari la rezumat](#rezumat)**

### Problema Numărul 1: Liderul Puternic

Din cele mai vechi timpuri liderul puternic era centrul decizional a comunității.
În viziunea strămoșilor noștri șeful avea rol absolut. Era omul care organizează și face totul posibil. Era omul care *salvează* tribul, cum deosebi putem observa la egipteni. Era omul care a fost dat de divinitate să reprezinte atât divinitatea cât și poporul. Omul cu cele mai multe drepturi dar și cu cele mai multe responsabilități.

Pe parcursul evoluției umanității perspectiva liderului puternic s-a schimbat considerabil. Uneori i-a crescut puterea. Alteori a scăzut. Anume uneori puterea devenit mai intensă printr-o formă absolutistă în care liderul națiunii avea putere maximală. Alteori și ca de exemplu dupa ridicarea democrației elene, puterea liderului națiunii a scăzut.

Odată cu creșterea complexității vieții oamenilor, de exemplu începând cu epoca medievală unele libertăți umane au început să crească ușor-ușor cum a fost de exemplu comerțul.
Odată cu creșterea libertăților a crescut și tendința de auto-organizare descentralizată. Dorința de a râmâne fără conducere și impunerile autorității centrale. A crescut dorința de a-ți rezolva lucrurile pe pâmântul tău cum dorești fără să fie nevoie să dai vreun tribut. A crescut oarecum individualismul și dorința de a păstra ceea ce produci și anume o auto-suficiență economică. Dacă ar fi să o numim așa.

Dacă sistemul liderului puternic e atât de bun, de ce ajunge de fiecare dată distrus de o revoluție? De ce sistemele democratice cu separația puterilor în stat nu au revoluții?

### Răspunsul e simplu și e sub nasul nostru deja de o mie de ani: ***Liderul puternic nu te va ajuta veci pururi.***

Desigur! Vreo câțiva ani își va face treaba și totul bine și lejer. Dar întotdeauna acel *puternic lider* care e și șef absolut inevitabil va ajunge să abuzeze de un aparat de conducere în vreun fel sau altul indiferent dacă este în limitele legii sau nu. O să zică mulți: *Dar poate nu abuzează*

Nu există *nu abuzează* când e vorba de conducere și asta e realitatea cruntă pe care Europa a descoperit-o dupa multe secole de *conducere a liderului puternic*
Realitatea asta cruntă încă nu ne-a lovit pe noi deoarece locuitorii acestui pâmânt (strămoșii noștri) am fost sclavii imperiilor. Noi nu am avut niciodată puterea militară de a ne organiza singuri. Nu aveam cum să cunoaștem libertatea de a te organiza singur.

### *Jugul imperiilor ne-a învățat o prostie de care majoritatea românilor ține cu dinții: „Liderul puternic te va conduce drept și îți va rezolva toate problemele din casă și societate”*

În timp ce popoarele vest-europene învățau generație cu generație moduri noi de scăpare de sub jugul liderului puternic cu puterea absolută, popor român încă nu cunoștea libertatea de a se organiza singur.

Astfel ajungem la **problema 2** cât și acest grafic:

![](/conducerea-liderului2.png)
###### Sursă: European and World Values Surveys

## Problema numărul 2: Poporul nu se știe organiza singur

Nu există nici un fel de dubiu că la o națiune care se asteaptă de la un lider puternic să facă totul oamenii nu se știu organiza singuri.

Dar de unde a pornit și incapacitatea asta? Că din ce vedem chiar și rușii o au! Poate la ei e chiar mai pronunțată.
Europa nu a fost dintotdeauna democratică ci chiar din contră: **a creat cele mai criminale regimuri dictatoriale.**

Europa a fost de atâtea și atâtea ori extrem de aproape de dictaturi. E ironic! Deoarece tot în Europa s-a născut democrația pură. Adică nu democrația reprezentativă de astăzi cu care toți suntem familiari și care e un oarecare eșec.

Pe parcursul istoriei Europa a învățat încetul cu încetul faptul că nu ai absolut nici o garanție că un rege absolutist sau un dictator nou își va face treaba față de popor. Nada.
Niciodată nu vei avea garanția că un om pus prin mecanisme dictatoriale nedemocratice își va face treaba sau va respecta libertățile tuturor în momentul în care este pus în funcție cu toată puterea regatului sau a națiunii.

Luăm exemplul lui Gorbaciov. A fost pus drept liderul Uniunii Republicii Sovietice Socialiste în anul 1985. Printre primele reforme pe care le-a luat a fost să desființeze toată structura dictatorială de partid peste societatea rusă. Automat, s-a folosit de putere ca să distrugă puterea. A fost omul care a desființat imperiul malefic (URSSul) și omul care a reintrodus reformele democratice pe întreg teritoriul uniunii. A fost un miracol. Miracol căruia îi datorăm astăzi libertatea și viața.

De unde puteau ști rușii că vine chiar un democrat pe cel mai înalt scaun a puterii? Nada, din nou. **Niciodată nu vei avea de unde ști cine vine pe un post de conducere neales**. Nu există nici o garanție că puterea va cădea în mâinile unui om bun. Niciodată. Umanitatea este făcută să greșească. Uneori intenționat, alteori neintenționat.

Cu prea multă putere în mâinile a prea puțini oameni [unele greșeli pot rade de pe fața pâmântului miliarde de suflete](http://www.latinamericanstudies.org/cold-war/sovietsbomb.htm).

Și ajungem la cel mai important punct al articolului: **Liderul puternic dăunează prin existența atribuțiilor decizionale mult prea sporite și a puterii absolute peste limitele democratice, peste libertate și peste separație a puterilor în Stat.**

Cei care citiți acum asta vă gândiți (probabil): [*„Dar Dan Barna a propus ca președintele să aibă puteri sporite”*](https://cursdeguvernare.ro/dan-barna-si-a-anuntat-programul-politic-puteri-sporite-pentru-presedinte-alegeri-locale-in-doua-tururi-si-reducerea-numarului-de-parlamentari.html). Exact. Și asta este o greșeală tipică românească! Uluitor transmisă tineretului! Face exact ceea ce se zice despre români în graficul de mai sus. Este exact ceea ce ne ține neamul în pământ. Este o greșeală istorică, aproape pecetluită să se repete.

În momentul în care tendința actuală a poporul român e spre dictatură, cum îți permiți să o aperi? Am auzit argumentări odioase de genul: *”Dar Dan Barna nu e așa... nu e dictator... și nici Iohannis...„*

Da. Așa este. Ei **probabil** nu sunt. Dar dacă ar fi participat Dragnea la alegeri prezidențiale și le-ar fi câștigat? Ce-ai fi zis atunci? Pot să ghicesc?
„***Trebuie să reducem atribuțiile prezidențiale cu ajutorul Parlamentului!***” cam asta aș fi auzit din gura oricărui USRist în acel moment. Putem paria, nu-i bai.

Democrația va funcționa întotdeauna când puterile conducerii se reduc în favoarea responsabilității (sau chiar și auto-responsabilizării) indivizilor, cetățenilor. Altfel, nu funcționează. Nu ne putem alege dictatorul la fiecare 4 ani și să ne așteptăm [să nu ajungem ca în situația Turciei cu sultanatul modern.](https://www.dw.com/ro/erdogan-primul-sultan-al-turciei-moderne/a-44581336).

Și aici vine în sfârșit **[tradiția obștilor sătești](https://adevarul.ro/locale/focsani/de-fost-considerate-obstile-satesti-cea-mai-stralucita-forma-democratie-functionau-beneficii-aduceau-1_56c1b0035ab6550cb8d29ac7/index.html)** distrusă de imperii și timp. Aveam și noi odată și-odată ceva care era cu adevărat strălucit comparat cu alte organizări descentralizate locale a vremurilor de pe atunci.

Venirea comuniștilor a determinat distrugerea tradiției românești de obști sătești. Bunicii care încă trăiesc habar nu au ce e aia obște sătească. Auzeau de ea doar pe la știri când mai ardeau comuniștii pe rug vreun *rezistent* al vremurilor.

În momentul în care nici macar bunicii nu mai știu să se organizeze împreună cu comunitatea lor locală, **cum bunul D-zeu să ne așteptăm să mai știm ce-i democrația și auto-organizarea locală inclusiv prin obști sătești?**

**Cum bunul D-zeu să ne așteptăm să nu fim incapabili în momentul în care noi am uitat să ne organizăm singuri local fără ajutorul autorității centrale?**

Nu contează cum te organizezi (după cum am început postarea) în firme locale care să colaboreze cu obștea locală, în organizații non-profit care să serveasca interesul celor care le mențin, în [cooperative private](http://monthlyreview.org/2015/02/01/cooperatives-on-the-path-to-socialism/) locale care să creeze producție comună de bunuri. Dar măcar ceva care să-ți dea educația să întelegi comunitatea. Aparent poporul român pare să fie de zeci de ori mai individualist decât americanul și de zeci de ori mai prost organizat.
Măcar ceva care să-ți dea cunoștințele de bază că dintr-un [virus](https://www.cdt-babes.ro/articole/coronavirus-infectia-COVID-19.php) chiar poți muri, [inclusiv sănătos să fii](https://www.digi24.ro/stiri/actualitate/un-sofer-de-tir-roman-care-intreba-pe-facebook-daca-a-vazut-cineva-un-mort-de-coronavirus-a-murit-din-cauza-virusului-1290744).

Dar ce e cel mai important de înțeles...

# Rezumat

* Orice lider cu prea multă putere va fi mereu dăunător. Funcția e de vină prima, după aceea omul. Funcția corupe întâi și întâi.

* Democrația funcționează doar dacă atribuțiile conducătorilor sunt reduse (unele chiar eliminate), nu invers!

* Democrația e cea mai bună formă de organizare în acest moment. Dacă o elimini, nu ai garanția că vine un lider care-ți convine. **Faptul că poți schimba un nebun prin vot, e un dar Dumnezeiesc.**

* Dacă poporul vrea să-și revină, trebuie să invețe să se guverneze local mai eficient decât s-ar organiza cu ordine de la centru

* Liderii votați ar trebui să nu mai aibă putere mai multă decât votul majorității, iar atribuțiile lor trebuiesc reduse!

* Liderii politici nu trebuie să aibă putere absolută.

* Privește atribuțiile oricărei funcții de conducere cu scepticism. Analizează atent structura conducerii.

* Orice dictatură inevitabil o să moară odată și-odată. Nici un rege nu trăiește pe veci.

* Mai citește câte-o vorbă despre obști sătești, poate-ți vine vreo idee de-o cooperativă cu vecinu`