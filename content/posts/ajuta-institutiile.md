---
title: "Ajută instituțiile a căror slujbă este să ajute oamenii!"
date: 2020-04-14T04:22:57+03:00
draft: false
tags: [ "INFORMAL", "OpiniePersonala", "In curs de revizuire" ]
---

**Atenție! Aceasta este o postare informală!**

Am văzut tot felul de dubioșenii de postări zilelea acestea despre ineficiența sistemului de sănătate. Cele mai multe postări aveau cea mai mare dreptate: sistemul de sănătate este momentan ineficient. Fără nici un dubiu.

<!--more-->

### Dar vreau să înțeleg: cum s-a ajuns aici?

Putem începe să înțelegem un pic uitându-ne la modul cum a fost organizat sistemul de sănătate în regimul socialist de dinainte de 1989: **Cu forța. Nu prin înțelegerea scopului (medicinei)**

Nimeni nu a înțeles prea multe. De ce trebuie să ajutăm oamenii dacă suntem plătiți cu un salar. De ce nu trebuie să luăm mită daca avem un salar asigurat. De ce aia, de ce aia...

Oamenii își făceau treaba la lumina cărei lămpi puteau cu orice bucată de pâine aveau la îndemână în timp ce sistemul îi prostea ca totul merge bine. Că țara prosperă. Că făceau muncă patriotică plătită din banii partidului. Banii mulți și serioși. Partidul le umplea creierele cu propagandă că doar partidul le poate asigura viața, banii și mâncarea.

Odată ce partidul a crăpat și națiunea a scăpat de sub jugul imperial sovietic sistemul medical s-a trezit aproape inutil. Nu mai întelegea niciunu ce scop mai are prin Medicină. Toți au putut să observe faptul că a venit capitalismul și fiecare trebuia sa producă ceva.

Medicii în viziunea comunistului **[boomer](https://ro.wikipedia.org/wiki/Boomer)** cică ar fi cea mai bine plătită meserie și tot așa a ținut-o de la răposat încoace. Practic ironia sorții a fost că ideea de medicină a ajuns adânc însămânțată în capul românului ca fiind cea mai bună și profitabilă meserie dintre toate. Meserie care-ți aduce (vezi Doamne) valoarea.

Uluitor și fără nici o surprindere bătrânul boomer a reușit să creeze o valoare dintr-o funcție bugetară instituțională și să-i dea adaos comercial până când doar adaosul comercial mai conta. Boomeru' a reușit să distrugă scopul medicinei și a doctorilor: **a ajuta oamenii**.

Asta este meseria bugetarului din sistemul medical: **a ajuta oamenii indiferent de condiții pentru că asta ești plătit sa faci**, **asta trebuie să îți fie vocația dacă ai decis să-l asculți pe boomerul dus cu pluta.**

Ghiciți ce s-a întâmplat cu sistemul de sănătate. Evident: s-a umplut cu brokeri pe post de doctori. Cine i-ar putea condamna din moment ce boomerul sovietic propagandist și-a făcut treaba cu eficiență și rigoare excepțională?

**„Du-tye la Medicină! Acolo's bani muuulț!”**, suna boomerul în urechile tinere ale generațiilor pierdute de după 1990.

**„Du-tye unye's bani muuulț!** (mulți de *uuu* aici) **Lasă tu pasiunea! Dă-ț-o dracului! La șe-ai nevoie de ea?”**

În schimb nu putem zice absolut nici o clipă faptul că întreg sistemul medical este așa. Am văzut doftori excepționali. Doftori cu adevărată vocație. Le ofer tot respectul. Am văzut pe săracii pasionați de medicină cum aproape că-și dădeau lacrimile non-stop văzând propria muncă distrusă și călcată în picioare de oameni fără respect. Și ba mai mult: oameni care-i vor afară pe cei buni și pasionați din cauza celor care au intrat în sistemul medical pe modulul sovietico-boomer (adică din sfatul și *pila* părintească).

## Și atunci mă gândesc: merită să fie desființată o instituție care are 3 medici buni și 5 medici falși?

Mă gândesc cât se poate de umil: este imoral să-l vreau afară pe medicul fals care trăiește de pe banii mei dar să mențin instituția pentru cel care-și face treaba?

Răspunsul zace din nou în modul nostru personal de gândire ca și neam.

Unii ar zice fără echivoc: **desființare**. Alții ar zice dictatură militară. 

Suntem campionii soluțiilor extreme dar niciodată nici o soluție extremă nu ne-a funcționat. Pur si simplu. Uite-te cititorule, la spitalele private și reacția lor la întâlnirea verbală cu COVID-19: **unele și-au închis spitalele**. Ei n-au nevoie de COVID-19. Deci nu, bogdaproste.

## Soluția pe care o propun și pe care o văd aplicată în câteva locuri prin țară: ***Ajută instituția măcar pentru cei plătiți care-și fac treaba!***

Știu, știu. O să ți se pară că sunt ipocrit. Că propun să repari fix ceea ce am criticat mai sus. O să zici ca sunt relativ nebun.

Dar nu, nu asta e cauza. În momentele astea întunecate singurele speranțe zac în spiritul comunitar a oamenilor. În puterea și voința de a ajuta benevol în limita posibilităților.

Întai ajuți și după aceea te decizi dacă mai vrei desființarea sistemului medical sau nu. Dacă mai vrei dictatură militară în sistem medical sau nu. Nu acum, acum e moment crucial. Dar după vei avea vreme îndeajuns de multă încât să stai și să te gândești.

## Orice instituție publică există ca să ajute oameni. Prin oameni.

Indiferent de sistem (medical, educațional, administrativ, etc..) Noi ca și cetățeni pentru ca să eficientizăm instituțiile publice trebuie întâi și întâi să le oferim ajutorul să se eficientizeze.

Nu este cu nimic contra-intuitiv să ajutăm (de exemplu) sistemul medical să reducă din costuri și din oameni care trag în jos instituția.

Nu este cu nimic greu și imposibil să ajutăm instituțiile cu softuri libere ca să își eficientizeze partea digitală și infrastructura digitală.

Iar influența politică și numirea politică trebuie să iasă din instituțiile publice. ***Politicul nu mai trebuie să numească conducerea instituțiilor publice.***

Dar acest subiect în particular îl voi trata la [**Ideologii Politice**](https://piratul.ro/tags/ideologii-politice), cât și la [**Transparența Guvernamentală**](https://piratul.ro/tags/transparenta-guvernamentala).

Atâta timp cât o instituție are nevoie de ajutorul tău pentru o acțiune ce beneficiază publicul este moral să o ajuți și să accepți cererea de sprijin.

Cum poți ajuta? Te informezi prin autorități. Suni la ei și îi întrebi. Unde este nevoie de sprijin, ce fel de sprijin, cum poți oferi sprijin, ș.a.m.d.
Faptul că ajuți, ajuți să ieșim din situația complicată și automat reîncepem producția. Implicit repornim economia.

Să latri de pe bancă la cât de ineficient a fost Statul și cât de varză au fost instituțiile până acum e ca și cum ai zice ca *oricum nu mai ești interesat de revenirea economică, socială și psihologică*

Dacă ajuți, ajuți să scapi într-un final de acei brokeri ajunși doctori și puși de părinții boomerși sovietici pentru profitul și valoarea familiei.


( Dacă te-a ofensat ceva te rog frumos să nu mai citești vreodată ce scriu. Mulțam! )