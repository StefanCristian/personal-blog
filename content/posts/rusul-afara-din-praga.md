---
title: "Rusul sovietic afară din Praga"
date: 2020-04-12T18:28:36+03:00
draft: false
tags: ["INFORMAL", "Libertate", "OpiniePersonala", "Ideologie Politica", "In curs de revizuire"]
---

**Atenție! Aceasta este o postare informală!**

Piratul politic ceh îl vrea pe rusul sovietic afară din Praga iar eu aplaud!

<!--more-->

---

Mă refer la această veste: https://www.reuters.com/article/us-russia-czech-statue-idUSKCN21S168

Scriam zilele trecute despre ideologia pirat, despre [libertăți](https://piratul.ro/static-post/libertate/) și detalii de genul. Toate bune și frumoase.

Îmi iese mie [cumva ceva idee](https://piratul.ro/static-post/libertate/) acolo despre libertăți și relația cu pirații politici. Până ce dau de o știre care mă oprește pe loc din scrisul și revizuitul postării. În acel moment am uitat complet de postarea cu libertățile în viziunea piraților. Mi-am întors ochii direct spre știrea care-mi stătea în fața ochilor și cu un zâmbet până la urechi citesc: Praga vrea afară cu statuile sovietice din țară!

Dar cum se poate? Cică ar întreba mulți, să scoți afară o statuie sovietică din țară? Din ce țineam minte mulți sugerau că toate statuile cu eroi sovietici au fost demolate deja. Poate greșesc, poate țin minte prost. Dar unii nesimțiți încă se gândesc la *salvatorii sovietici*, la marii *eliberatori* care după ce ne-au scăpat (har Domnului) de național-socialiști, ne-au călcat în picioare libertatea și insăși viața.

Partidul Pirat din Cehia, reprezentat de primarul pirat din Praga, dorește să scoată afară toate statuile de *eroi* sovietici asupritori din țară.

Această veste nu doar că ar trebui să ne bucure, dar dovedește incă o dată faptul că flacăra de libertate va rămâne în ideologia politică pirat chiar dacă uneori unii pirați doresc măsuri mai dure în societate cu ajutorul reglementărilor legislative. Atâta timp cât democrația directă din interiorul partidului pirat rămâne în picioare inevitabil pirații politici își revin.

Revenind la vestea cu statuia sovietică. Mă bucur enorm că Rusia este infuriată. Dovedește incă o dată faptul ca democrația incă nu s-a inrădăcinat în societatea lor. 

Dar totuși mă întristează în același timp faptul că democrația nu s-a inrădăcinat în societatea lor. Rusia **putea** fi democratică. Putea fi mai puțin impunătoare pe scară europeană și globală. Așa s-au decis rușii. Iar noi ca și pirați politici suntem fundamental opuși deciziei lor actuale.

Rusie dragă, ne bucurăm că ești frustrată de faptul că un pirat îți scoate afară istoria de asupritor **din propria lui țară!**

La mai mult!


*P.S. Am revenit la revizuitul postării cu libertățile imediat după toată știrea cu scoaterea statuilor de sovietici afară din Praga.*

*Dacă mori să înțelegi de ce adaug detalii de genul la începutul și sfârșitul postării, trebuie să citești un pic despre definiția unei postări [informale](https://www.skillsyouneed.com/write/formal-or-informal.html)*

**Autor: Ștefan Cristian Brîndușă**