---
title: "Partidul Pirat Ceh pe primul loc la sondaje parlamentare în Cehia"
date: 2021-02-14T18:04:48+02:00
draft: false
---

<!--more-->

<a href="/piraticehi1.jpg" target="blank"><img class="img-post" src="/piraticehi1.jpg"/></a>

Sursa: Kantar CZ

Pentru prima dată în mișcarea Pirat, un partid politic pirat ajunge prima opțiune politică dintr-o țară europeană.

Partidul Pirat Ceh a preferat să facă o alianță cu o formațiune politică localistă-conservatoare, ca să scoată afară liberalii (ANO-Renew), socialiștii și comuniștii de la guvernare.

Partidul Pirat Ceh și ca de altfel aproape toate partidele pirat, sunt în afara spectrumului politic stânga - dreapta, de regulă le găsești la centrism anti-autoritarism.

Praga de exemplu și-a votat primar din partea Partidului Pirat Ceh, fiind prima capitală europeană cu primar Pirat.

Organizația Internațională a Piraților politici: https://www.facebook.com/ppinternational/posts/4414347865258240
