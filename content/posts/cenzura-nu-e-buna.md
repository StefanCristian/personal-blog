---
title: "Site-ul ortodoxinfo.ro a fost cenzurat. Deși momentan este justificată cenzura, abordarea este incorectă."
date: 2020-04-26T08:40:31+03:00
draft: false
tags: [ "INFORMAL", "OpiniiPersonale", "Libertate" ]
---

**Site-ul ortodoxinfo.ro a fost cenzurat pe teritoriul României de către ANCOM.**

<!--more-->

Mai precis mă refer la această **[veste](https://www.digi24.ro/stiri/sci-tech/lumea-digitala/site-ul-ortodoxinfo-ro-inchis-pentru-fake-news-se-pregateste-omorarea-pensionarilor-in-lagare-de-concentrare-si-exterminare-1297481)**

Site-ul a fost interzis pe teritoriul României prin ordin de la ANCOM pentru o știre nu doar dubioasă dar de-a dreptul ridicolă: „***Se pregătește omorârea pensionarilor în lagăre de concentrare și exterminare***” (titlul vechii știri). Din păcate doar atât am din știrea din ortodoxinfo, dar titlul e destul de orientativ.

Deja văd o [droaie](https://dexonline.ro/definitie/droaie) de oameni care se bucură de cenzurarea unui site care răspândește știri ~~imbecile~~ false.

Bineînțeles că îi înțeleg pe deplin: se bucură de impunerea filosofiei similare lor.

#### Dar, este greșit să cenzurăm un site de știri false care oricând poate să revină sub orice altă formă dorește!

Nu trebuie să se numească *ortodoxinfo.ro*, poate să fie *ortodox.info.ro*

Singurul lucru care s-a schimbat aici este exact ceea ce pățește orice om cenzurat: *i-a fost admisă și împuternicită existența*.

Acești oameni sau mai de grabă zis *acest om* care publica pe ortodoxinfo.ro nu a primit altceva decât un șut în posterior ca să continue cu aceeași retorică dubioasă ca și înainte. Cenzura nu l-a schimbat cu absolut nimic. Cenzura doar l-a ajutat să creadă în continuare că are dreptate, că este persecutat de ***sistemul opresiv***

Chiar și eu dacă aș publica știri extremiste cu ***Distrugeți toate drepturile de autor*** și aș fi cenzurat pe teritoriul României de către ANCOM cu siguranță nu m-aș da bătut. Ci, chiar din contră: ***aș simți că am dreptate***.

Ei bine, fix asta s-a întâmplat aici. Omul chiar crede că are dreptate:

![](/ortodoxinfo2.png)


Deci ce-au făcut ANCOM? Un sfânt ~~căcat~~ nimic.

Ia, îs deja *12.591* creduli. Cum să nu-ți fie milă de ei?

## Nu ai cum să oprești știrile false cu cenzură.

## Nu ai cum să oprești știrile false fără să ai educație și sistem educațional.

În această perioadă legile permit cenzurarea site-urilor de știri false. Deci cenzurarea oricărei știri false este justificată prin ordonanțele militare.

Dar ce faci după? Lege specială de *control al internetului*? De *control a conversațiilor pe internet*? Dacă scrii la mișto un blog vine ANCOMul să te ia? *Inteligență artificială* care să scaneze după tipare?

## Nu ai cum. Trebuie Educație. Educație, educație, educație! Nu cenzură.

Înțelegerea site-urilor de știri false prin educație este singura cale prin care astfel de site-uri își vor pierde din relevanță din cauza faptului că utilizatorul de internet va fi îndeajuns de pregătit să înțeleagă faptul că știrile sunt false.

Însă, nimic nu poate opri un utilizator să aibă acces la aproape orice site-uri pe Internet.

Cel mai probabil acum autorul ortodoxinfo va scrie despre VPN și TOR. Sau va scrie despre cum poți trece de cenzură pe site.

Dar cu alte cuvinte concluzia mi-e destul de simplă: **Consider această abordare (prin cenzură) greșită. Cenzura nu va rezolva vreodată problema de fond.**

### Pentru o definiție mai largă a cenzurii aveți acest site care vă explică în engleză (din păcate în engleză e mai bine pus la punct) în ce forme se manifestă o cenzură prin ordin guvernamental online: https://en.wikipedia.org/wiki/Internet_censorship.

Blocarea unor domenii (indiferent de criterii) cade sub incidența cenzurii: *„As per Hoffmann, different methods are used to block certain websites or pages including DNS poisoning, blocking access to IPs, analyzing and filtering URLs, inspecting filter packets and resetting connections.”* (..) *„Domain name system (DNS) filtering and redirection: Blocked domain names are not resolved, or an incorrect IP address is returned via DNS hijacking or other means„* din categoria de *„Content suppression”* (suprimarea conținutului de pe site-uri).

Cenzura prin definiție este guvernamentală deoarece în relațiile contractuale private atât producătorul cât și consumatorul se angajează la un contract privat unde stipularea de restrângere a unor drepturi este clar definită și fără influență guvernamentală sau legislativă ca de exemplu ordin militar. Însă domeniile înregistrate la agenția națională de înregistrare a domeniilor în Romania ROTLD.RO (adică o autoritate națională cu regim public) contractul este de natură publică și nu există stipulare de restrângere a dreptului de libertate de exprimare pentru conținutul unui site decât dacă este vorba despre pornografie infantilă sau conținut terorist. Spre deosebire de contractele private, contractele cu autoritățile publice stipulează faptul că ceea ce semnezi și ceea ce primești poate fi *schimbat* în funcție de ordonanțe de urgență sau legi noi. Cu alte cuvinte, poți fi cenzurat prin blocare sau redirecționare de domeniu dacă apare un ordin militar care stipulează că știrile *considerate* false pot fi blocate (adică cenzurate).

Ceea ce a făcut ANCOM respectând ordinul guvernamental este cenzură. Justificată, într-adevăr, dar cade la definiția de bază a cenzurii.

Din acest motiv dacă înregistrezi domeniul în afara țării cumva se presupune că ai mai multă libertate.

###### *Pot să bage în ordonanță o obligație temporară prin care forțează site-urile cu știri false să scrie efectiv pe site-ul propriu că sunt site de divertisment. [Just sayin' for the fun](https://translate.google.com/?um=1&ie=UTF-8&hl=en&client=tw-ob#auto/ro/just+saying+for+the+fun).*