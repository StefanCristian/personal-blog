---
title: "Bipolaritatea (sau ipocrizia?) românească cu site-urile de torrente pirat"
date: 2020-04-30T10:19:53+03:00
draft: false
tags: [ "INFORMAL", "OpiniePersonala", "Libertate", "Drepturi de Autor", "LaMisto" ]
---

Românul este cât se poate de ipocrit și bipolar atunci când vine vorba de piraterie și site-uri cu torrente pirat.

Deși nu cunosc un studiu anume care să dovedească asta, din toate discuțiile pe care le-am purtat cu destui oameni, cât și nenumărate postări publice pe Facebook, pot să observ această atitudine.

De asemenea, e ceva evident că această atitudine se manifestă la doar o parte din români, nicidecum la toți.

<!--more-->

Pe lângă faptul că nu vezi prea mulți oameni care înțeleg faptul că **torrentele nu sunt ilegale**, ci conținutul pus la dispoziție de utilizatori prin [trackere](https://www.howtogeek.com/141257/htg-explains-how-does-bittorrent-work/). Dar nu despre asta vorbesc, ci despre *torrentele pirat*. Adică acele torrente care duc spre conținut ilegal.

Zilele astea am auzit de-o panică uriașă că, vezi Doamne, a crăpat un site de torrente pirat.

Bineînțeles că nu a crăpat. Un site nu se retrage așa ușor. Ci, mai degrabă s-a mutat.

Dar nu despre asta mă interesează să discut acum.


### Am observat un fenomen latent de susținere a unei *presupuse* tradiții românești când vine vorba de anumite site-uri cu torrente pirat

Și mă fascinează că deși *se presupune* că a devenit tradiție să susții site-urile cu torrente pirat, peste tot pe unde mă uit citesc: „*bă... nu e ok, totuși, pentru autori...*”, „*bă... download-ul ăsta de pe torrente nu e bun, ar trebui sa fie ilegal...”*.

În același timp vezi aceeași oameni care susțin site-urile cu torrente pirat *naționale* cot la cot cu condamnarea pirateriei.

Mă fascinează tot în același timp că aceeași oameni trag de pe torrente pirat în mod inconștient, poate chiar debil, și vin după să ți se laude la ce mult susțin ei proprietatea intelectuală.

Explică-mi și mie ce-i cu ipocrizia asta? Sau, mai de grabă zis ce e cu combinația asta de schizofrenie, ipocrizie și bipolaritate?

Nu mă deranjează dacă îmi zici că „*bă, o singură dată să ai dreptul să downloadezi gratuit e ok și în rest nu mai, gata.*”

Accept genul ăsta de argumentare! Nu-i nici un bai! Doamne ferește, toți avem dreptul la opinii!

Dar nu vii la înaintare cu explicații că ce bine e că avem site cu *torrente* (mă refer la torrente pirat, [nu cele legale!](https://www.giz.ro/internet/ce-sunt-torentele-si-cum-pot-sa-le-descarci-si-tu-2532/)) și în același timp condamni ferm pirateria. N-are absolut nici o noimă, nici o logică, nici un pic de bun simț.

### Genul ăsta de oameni trebuie să se hotărască: susțin site-urile de free-sharing nerestricționat (inclusiv pirat) sau NU.

Nu mă interesează care îți este opțiunea personală. Ori susții, ori nu!

Nu mă deranjează dacă vii și zici: *Bă, nu susțin pirateria și download-ul de conținut fără permisia producătorului/autorului.*

Este opinia ta și nu te pot obliga să susții nimic din ce susține oricine altcineva.

În schimb, există o soluție cu toată *pirateria* si free-sharing-ul ăsta nerestricționat și *presupusa* tradiție românească de *piraterie*, dar aparent e prea greu de explicat pentru unii.

Încerc să o simplific așa:

### Nu este cu nimic imoral să susții existența unui site *non-profit* pentru răspândirea fișierelor de tip *.torrent* care conectează oamenii ce fac sharing și oamenii care descarcă de pe net prin program cu torrente pirat

Am încercat să țin cât se poate de simplistă explicația deoarece nu tot publicul este *tehnic*.

Este o definiție parțială și nicidecum completă. Ar trebui să intru în mult mai multe detalii, însă nu în această postare.

### Am observat că acest fenomen de ipocrizie și bipolaritate românească se manifestă în jurul site-urilor *non-profit* de free-sharing / free-download

Dacă într-adevăr susții aceste site-uri de *free-sharing* nerestricționat și în același timp vrei condamnarea pirateriei cot la cot cu susținerea drepturilor de autor / proprietății intelectuale, atunci asta s-ar putea să fie singura opțiune de mijloc: 

* **Site-urile de free-sharing de conținut pirat să nu facă vreodată profit de pe produsele autorilor**

Este singura opțiune de mijloc la care mă pot gândi și care i-ar putea ajuta pe cei cu ipostaza de pro și contra pirateriei concomitent.

### Astfel vin cu această *posibilă* soluție care se poate adapta prin aceste idei. Ca să nu pari ipocrit și bipolar atunci când te intreabă cineva dacă ești pro sau contra descărcării de pe site-uri cu torrente:

* Poți susține existența unui site *non-profit* de torrente. Un site *non-profit* se referă la imposibilitatea deținătorilor site-urilor de a face bani de pe operele altora!

* Poți susține existența aceluiași tip de site *non-profit* în condițiile în care cei care încarcă pe site **dețin licență** pentru operele cumpărate! Chiar dacă le pune la dispoziția publicului prin site-ul *non-profit* de torrente asta nu înseamnă că trebuie să încarce opera pe site fără să o dețină în mod legal.

* Poți susține introducerea de limitări de descărcare sau limitări de încărcare pe site-uri de torrente *non-profit* pirat. Ca de exemplu un utilizator are dreptul la 10 descărcări pe lună sau 10 incărcări pe luna. Nu e obligatoriu să susții interdicția site-urilor de torrente *non-profit* pirat. 

* Nu e este obligatoriu să susții ca aceste site-uri să fie neapărat publice. Pot să fie și private pe bază de invitație.

* Bineînțeles că ceea ce propun nu există din punct de vedere legislativ. Ar fi fost foarte mișto să existe. Dar asta nu te oprește să susții schimbarea legislației ca să ducă în această direcție!


Asta te va ajuta să nu fii considerat ipocrit dacă susții site-urile *non-profit* cu torrente pirat și în același timp să condamni pirateria.

Daca nu ești de acord trebuie să știi că nu mă deranjează dacă susții inclusiv menținerea în afara legii a site-urilor de genul. E strict opinia ta. Dar, te rog, să nu te aud cu *„e bine un pic de piraterie”* fără să susții propuneri măcar *similare* celor scrise mai sus.


##### Nu uita [poziția mea despre încălcarea legii și piraterie](/posts/aici-nu-e-piraterie/) dacă tot ai citit ce-am scris
##### Nu uita de etichetele de [#INFORMAL](/tags/informal) și [#OpiniePersonala](/tags/opiniepersonala)