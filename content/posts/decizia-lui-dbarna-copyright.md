---
title: "Uluitor, Dan Barna nu s-a folosit de drepturi de autor ca să cenzureze"
date: 2020-04-14T17:58:41+03:00
draft: false
tags: [ "INFORMAL", "OpiniePersonala", "Drepturi de Autor" ]
---

**Atenție! Aceasta este o postare informală!**

Vine ca și o surpriză dar uite că se poate: [Dan Barna **nu** a cerut Facebook-ului scoaterea videoclipurilor amuzante cu el care bate la tastele invizibile](https://www.facebook.com/DanBarnaUSR/posts/829766624100303) de deasupra laptopului.
Bineînțeles că cel mai probabil nu din motiv de drepturi de autor Dan Barna nu a cerut Facebook-ului așa ceva. Dar cui ii pasă?

<!--more-->

## Anul viitor va intra în vigoare o ~~dictatură~~ Directivă europeană denumită Directiva Drepturilor de Autor în Uniunea Europeană.

Această directivă (ca și orice altă lege) se vrea a fi nobilă în esență și nobilă în aplicare. Dă-mi voie să-ți zic că amandouă considerente sunt absolut greșite: directiva asta nu e nici nobilă și nici aplicabilă.

Directiva va avea, printre multe altele, aceste două obligații:

### Obligația numărul 1: *Filtre digitale pentru deținătorii de firme cu 3 ani vechime și cu site-uri publice unde utilizatorii pot să facă upload de conținut. Filtrele au rolul de a cenzura conținutul presupus „piratat”. Filtrele deși nu e obligatoriu să le ai nu există altă posibilitate de abordare.*

### Obligația numărul 2: *Orice preiei de la agențiile de știri înregistrate oficial ca și agenții de știri ești obligat să ceri acord pentru **citarea** oricăror bucăți din știri, inclusiv titlul. Adică pe blogul tău personal nu poți pune citate din știri.*


*Le voi explica în alt articol, dar pentru moment contextul creat prin explicațiile de mai sus este îndeajuns*

Dacian Cioloș (din auzite și rumori) s-a pronunțat împotriva *actualei forme a directivei* dar aparent **[USRul s-a declarat FERM împotrivă](https://devwww.usr.ro/2018/11/16/usr-cere-guvernului-sa-nu-sustina-propunerea-actuala-de-directiva-privind-modificarea-drepturilor-de-autor/)** sau cel puțin pentru momentul 2018. De atunci n-am mai auzit nimic.


Această directivă este menită să te oprească să mai dai vreodata copy-paste la un articol **public** de pe net fără voia celui care a scris articolul deoarece în viziunea unui autocrat copy-paste este distrugere de valoare și nicidecum răspândire organică. Tot această directivă este menită să te oprească să mai dai vreodată copy-paste & share la un link de YouTube a cărui cont nu este cel oficial al producătorului. ( Apropo! Dacă ești de acord cu așa ceva poți să te oprești aici si să dai Alt-F4 )

În viziunea unui autocrat și dat fiind faptul că Facebook oferă *cenzură* (mai corect zis este *blocare de conținut*) în numele drepturilor de autor, bineînțeles că este permis să ceri blocarea [videoclipului amuzant de pe net cu tine care bați aiurea deasupra tastelor](https://www.hotnews.ro/stiri-esential-23848500-video-tutorial-spalat-maini-dan-barna.htm).

Dar trebuie să înțelegi în acel moment că singurul lucru care e moral în ceea ce ceri este faptul că Facebook pot să decidă ce model de business folosesc. Dacă ei decid să nu-ți mai susțină ție drepturile de autor atunci *c'est la vie*! Respecți modelul lor de business și mergi mai departe.

### În momentul în care încarci ceva pe Internet pe un site public ( pe orice site care oferă un serviciu public ) te supui regulilor acelui serviciu. Te supui unui contract!

În acest caz politica Facebook pentru site-uri publice este: oricine are dreptul să copieze și să facă sharing ***FĂRĂ ACORDUL CELUI CARE A INCĂRCAT CONȚINUTUL PE SITE***. Dar ca să echilibreze modelul de business Facebook oferă în același timp opțiunea de a interzice conținutul și a-l retrage pe motive de drepturi de autor.

Sună schizofrenic dar așa este modelul lor de business. Tu ca și utilizator te supui că altfel Facebook îți sterge contul.

Bun. Am lămurit-o pe asta cu Facebook-ul. Dar ce facem în momentul în care un utilzator de Internet care nu are cont Facebook preia videoclipul **public** și-l trimite pe alte site-uri? Facebook scrie clar din nou: orice videoclip public ***este valabil public pe tot Internetul. Ceea ce este în afara site-ului lor nu intră în jurisdicția drepturilor utilizatorilor Facebook, nici a Facebook.***

Adică conform Facebook tu ai dreptul să faci copy-paste la un videoclip și să faci aproape tot ce ai chef cu el în limita obligativității de a menține, totuși, un citat atribuind *mulțumiri* sursei ca de exemplu: ***Creat de către (...)*** sau ***Copyright nume***

Automat nu există nimic care să te restricționeze să folosești videoclipul **în afara Facebook-ului**.

Eu până acum credeam că Dan Barna nu respectă decizia partidului din 2018. Îl vedeam ca și un soi de independent în partid, sau maxim influențăr. M-am înșelat. Totuși, respectă decizia partidului și filosofia din spatele deciziei partidului: USR nu vrea legi chiar în halul ăsta de restrictive la categoria de libertăți digitale. Nu mă așteptam nici măcar de la partid dar asta o discutăm altă dată.

Și aici ajungem înapoi la reacția lui Dan Barna. Cred că este conștient că din punct de vedere legal nu prea ai cum să elimini răspandirea videoclipului în afara Facebook-ului.
Nu știu sigur dacă dorește să-l elimine sau să ceară Facebook să-l *cenzureze*. Sau să-i *cenzureze* orice formă modificată. Habar nu am.

Dar un lucru e cert: De data asta Dan Barna a scăpat de critica mea :)