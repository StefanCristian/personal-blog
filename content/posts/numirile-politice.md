---
title: "Cum te momesc corupții să susții numirile politice?"
date: 2021-02-12T16:16:47+02:00
draft: false
tags: [ "INFORMAL"]
---

<!--more-->

Să zicem că ești din USR. Vrei să elimini corupția din țară și îți vine un coleg de partid și îți zice: ”*Singurul mod în care să eliminăm corupția din țară e să punem oameni de încredere în conducerea instituțiilor.*”

Tu întrebi: ”*Ce oameni de încredere?*”

El/Ea îți răspunde: ”*Poi oameni de încredere apolitici.*”

Bun, bun. Dar apolitici de-al cui?

”*Apolitici pe care-i cunoaștem ca fiind capabili.*”

Păi bine, bine, dar cine-i cunoaște ca fiind capabili?

”*Păi conducerea noastră din USR. Conducerea votată de membri.*”

.

.

Asta e momeala. Cam așa funcționează momeala și lipsa de bun simț în cercurile politice din țară. Ei presupun că va dispărea corupția dacă numești oameni capabili. Nimic mai greșit decât această presupunere. Atât oamenii capabili cât și cei incapabili vor servi nu interesele cetățenilor, ci a partidului. 

Nu o să te poți asigura că lucrează în slujba cetățenilor și nu a partidului. În schimb cei care te momesc cu *numiți politici cât de cât* capabili, este evident că inevitabil vor fi aserviți partidului. 

Un prieten de partid ar putea să pună o ospătăriță, pe când alt prieten de partid ar putea să scape câțiva oameni de dosar Microsoft.

Chiar dacă exemplul de mai sus din USR s-ar putea să nu fie real, oamenii din partide au metode foarte eficiente să te momească să crezi în numiri politice.

Am pățit chiar eu, în discuții, să fiu momit prin această propunere. Mi s-a pus pe masă o idee care pentru mulți ar fi tentantă: ”***Dacă ai avea un procuror pirat, ar putea fi tolerant la piraterie.***”

Exact asta e momeala. Încearcă să se folosească de opțiunea ta politică să te momească cu faptul că e nevoie de *tine, salvatorul țării* să fii cel care pune oamenii capabili în fruntea instituțiilor. Oameni capabili aserviți ție, dar *tu știi că nu ești corupt*, deci de ce nu ar fi cel numit (politic) în slujba cetățenilor? Că doar tu nu ești corupt.

Automat prin simplul fapt că tu, parlamentar sau președinte fără pregătire juridică și cu opțiune politică stabilită, decizi cine să conducă o agenție care investighează crime... ești deja părtaș la corupție. Cel numit nu are cum să fie imparțial. Cel numit nu va servi cetățeanul plătitor de taxe, ci un partid, o minoritate. Taxele tale se duc spre ei, spre o minoritate. În loc să se ducă spre competență, se duc spre o opțiune politică.

La fel se aplică și la situația cu [numiții politici din Administrația Națională a Apelor Române](https://www.g4media.ro/recorder-ro-un-animal-politic-a-angajat-o-fosta-chelnerita-pe-post-de-inginer-la-apele-romane-iasi-cu-cinci-ani-de-master-incerca-sa-deschida-calculatorul-apasa.html). Era evident că un om capabil va fi înlocuit de un pus politic care numește incapabili. Asta o vedeam de la sute de kilometri.

.

Cea mai evidentă momeală pentru oameni să susțină numirile politice este aceasta: ”**Dacă nu menținem numirile politice și lăsăm la concurs public, nu vom mai putea numi oamenii aleși de popor. Nu mai putem să ne asigurăm că dispare corupția.**”

.

Frate, **așa dovedești că dispare corupția!** Lași numirile prin **concurs public** pentru că e singura metodă să știm că omul care câștigă concursul e imparțial și capabil! E singura metodă!

.

**NU TE LĂSA MOMIT!**
